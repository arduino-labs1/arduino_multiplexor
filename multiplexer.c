#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <avr/io.h>
#include <util/delay.h>

void main() {
    DDRD  = 0b00000000;
    DDRB  = 0b00001110;
    PORTD = 0b11111111;
    PORTB = 0b00000000;

    int number = 0;
    while (1) {
	for (int i = 0; i < 8; ++i) {
	    if (!(PIND & (1 << i))) {
		number = i;
		break;
	    }
	}
	PORTB = number * 2;
	_delay_ms (100);
    }
}

#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <avr/io.h>
#include <util/delay.h>

void main() {
    DDRD  = 0b11111111;
    DDRB  = 0b11111000;
    PORTD = 0b00000000;
    PORTB = 0b00000111;

    while (1) {
	int number = 0;
	for (int i = 0; i < 3; ++i) {
	    if (!(PINB & (1 << i))) 
		number += (1 << i);
	}
	PORTD = (1 << number);
	_delay_ms (300);
    }
}

# Мультиплексор и демультиплексор
## Команды запуска:
```bash
$ avr-gcc -g -O1 -mmcu=atmega328p -o multiplexer.elf multiplexer.c
$ avrdude -v -patmega328p -c arduino -P /dev/ttyUSB0 -U flash:w:'multiplexer.elf'

$ avr-gcc -g -O1 -mmcu=atmega328p -o demultiplexer.elf demultiplexer.c
$ avrdude -v -patmega328p -c arduino -P /dev/ttyUSB0 -U flash:w:'multiplexer.elf'
```
